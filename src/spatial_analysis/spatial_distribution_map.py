'''
Created on Sep 18, 2022

@author: nejat
'''

from util_event import read_df_events, read_events_from_df, get_event_clusters_from_clustering_result
from plot.plot_event_distribution import prepare_all_event_distributions, plot_all_event_distributions, plot_all_event_comparison_maps

from util_event import read_df_events, read_events_from_df


import os
import consts
import pandas as pd
import csv


def perform_spatial_distribution_map(events_filepath_dict, output_dirpath, by_continent):
  
  #spatial_hierarchy_levels = ["country", "region", "city"]
  #spatial_hierarchy_levels = ["region"]
  spatial_hierarchy_levels = ["country", "region"] # 
  # no restriction on plot max limit value, so put all spatial hierarchy values
  #periods = ["All"]
  #seasons = ["All"]
  periods = [2020, 2021] # 
  seasons = ["summer", "spring", "autumn", "winter"] #  
  prepare_all_event_distributions(events_filepath_dict, output_dirpath, by_continent, seasons, periods, spatial_hierarchy_levels)
  plot_all_event_distributions(list(events_filepath_dict.keys()), output_dirpath, by_continent, seasons, periods, spatial_hierarchy_levels)
  #plot_all_event_comparison_maps(list(events_filepath_dict.keys()), output_dirpath, by_continent, seasons, periods, spatial_hierarchy_levels)
  #plot_all_local_moran_I_distributions(events_filepath_dict, output_dirpath, by_continent, seasons, periods, spatial_hierarchy_levels)
  

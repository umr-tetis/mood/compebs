'''
Created on Nov 16, 2021

@author: nejat
'''

import os

NEWS_SURVEILLANCE_PADIWEB = "padiweb"
NEWS_SURVEILLANCE_PROMED = "promed"
NEWS_SURVEILLANCE_HEALTHMAP = "healthmap"
NEWS_DB_EMPRESS_I = "empres-i"


# ====================================================
# FOLDER
# ====================================================

#MAIN_FOLDER = os.path.abspath("..") # the absolute path of the previous level
#MAIN_FOLDER = "/home/nejat/eclipse/tetis/compebs"
MAIN_FOLDER = "<YOUR_FOLDER>"

LIB_FOLDER = os.path.join(MAIN_FOLDER, "lib")
DATA_FOLDER = os.path.join(MAIN_FOLDER, "data")
DATA_SOURCE_GEO_COVERAGE_FOLDER = os.path.join(DATA_FOLDER, "news-websites-geography")

# input folder
IN_FOLDER = os.path.join(MAIN_FOLDER, "in")
IN_EVENTS_FOLDER = os.path.join(IN_FOLDER, "events")
IN_EVENTS_PADIWEB_FOLDER = os.path.join(IN_EVENTS_FOLDER, NEWS_SURVEILLANCE_PADIWEB)
IN_EVENTS_PROMED_FOLDER = os.path.join(IN_EVENTS_FOLDER, NEWS_SURVEILLANCE_PROMED)
IN_EVENTS_EMPRESSI_FOLDER = os.path.join(IN_EVENTS_FOLDER, NEWS_DB_EMPRESS_I)

IN_THEMATIC_TAXONOMY_FOLDER = os.path.join(IN_FOLDER, "thematic_taxonomy")

IN_MAP_SHAPEFILE_FOLDER = os.path.join(IN_FOLDER, "map_shapefiles")

IN_GEONAMES_INFO_FOLDER = os.path.join(IN_FOLDER, "geonames")

IN_NEWS_WEBSITES_GEO_FOLDER = os.path.join(IN_FOLDER, "news_outlets_geography")

# generic data folder
IN_MAP_SHAPEFILE_FOLDER = os.path.join(IN_FOLDER, "map_shapefiles")
IN_ENV_DATA_FOLDER = os.path.join(IN_FOLDER, "environmental_data")





# out folder
OUT_FOLDER = os.path.join(MAIN_FOLDER, "out")
# csv folder
OUT_NEWS_OUTLET_ANALYSIS_FOLDER = os.path.join(OUT_FOLDER, "newslet_outlet_analysis")
OUT_NEWS_OUTLET_ANALYSIS_PADIWEB_FOLDER = os.path.join(OUT_NEWS_OUTLET_ANALYSIS_FOLDER, NEWS_SURVEILLANCE_PADIWEB)
OUT_NEWS_OUTLET_ANALYSIS_PROMED_FOLDER = os.path.join(OUT_NEWS_OUTLET_ANALYSIS_FOLDER, NEWS_SURVEILLANCE_PROMED)
# event distribution
OUT_SPATIOTEMPORAL_ANALYSIS_FOLDER = os.path.join(OUT_FOLDER, "spatiotemporal_analysis")



# dataframe alignment
DATAFRAME_ALIGNMENT_FOLDER = os.path.join(OUT_FOLDER, "dataframe-alignment")

STATS_FOLDER = os.path.join(OUT_FOLDER, "stats")

ST_PATTERN_FOLDER = os.path.join(OUT_FOLDER, "st_pattern")
ST_PATTERN_PADIWEB_FOLDER = os.path.join(ST_PATTERN_FOLDER, NEWS_SURVEILLANCE_PADIWEB)

GEO_COVERAGE_FOLDER = os.path.join(OUT_FOLDER, "geo_coverage")

MULTIDIM_PATTERN_FOLDER = os.path.join(OUT_FOLDER, "multidim_pattern")
MULTIDIM_PATTERN_PADIWEB_FOLDER = os.path.join(MULTIDIM_PATTERN_FOLDER, NEWS_SURVEILLANCE_PADIWEB)

OUT_TIMELINESS_FOLDER = os.path.join(OUT_FOLDER, "timeliness")


OUT_GRAPH_FOLDER = os.path.join(OUT_FOLDER, "graph")
OUT_GRAPH_PADIWEB_FOLDER = os.path.join(OUT_GRAPH_FOLDER, NEWS_SURVEILLANCE_PADIWEB)



# ====================================================
# FILE NAMES
# ====================================================

PADIWEB_ARTICLES_CSV_FILENAME = "articlesweb"
PADIWEB_ARTICLES_INIT_CSV_FILENAME = "articlesweb_init"

PADIWEB_CLASSIF_LABELS_CSV_FILENAME = "classification_label"
PADIWEB_INFO_EXTRAC_CSV_FILENAME = "extracted_information"
PADIWEB_INFO_EXTRAC_INIT_CSV_FILENAME = "extracted_information_init"

PADIWEB_KEYWORD_CSV_FILENAME = "keyword"
PADIWEB_KEYW_ALIGN_CSV_FILENAME = "keyword_alignment"
PADIWEB_SENTENCES_CSV_FILENAME = "sentences_with_labels"
PADIWEB_EXT_SENTENCES_CSV_FILENAME = "extended_sentences_with_labels"
PADIWEB_SENTENCES_INIT_CSV_FILENAME = "sentences_with_labels_init"

PADIWEB_SIGNAL_CSV_FILENAME = "signal"
PADIWEB_DISEASE_KEYWORDS_FILENAME = "disease_keyword"


PADIWEB_AGG_SENTENCES_CSV_FILENAME = "aggregated_sentences_with_labels"
PADIWEB_RELEVANT_SENTENCES_CSV_FILENAME = "relevant_sentences_with_labels"
PADIWEB_EXT_INFO_EXTR_CSV_FILENAME = "extended_extracted_information"
PADIWEB_EVENT_CANDIDATES = "event_candidates_padiweb"
PADIWEB_EVENTS = "events_padiweb"
PADIWEB_INFO_EXTR_SPATIAL_ENTITIES_CSV_FILENAME = "extr_spatial_entities_info"



HEALTHMAP_ARTICLES_CSV_FILENAME = "articlesweb"
HEALTHMAP_SIGNAL_CSV_FILENAME = "signal"
HEALTHMAP_PATHS_CSV_FILENAME = "paths"
HEALTHMAP_PATHS_AGG_CSV_FILENAME = "paths_agg"
HEALTHMAP_PATHS_SIGNAL_CSV_FILENAME = "paths_signal"
HEALTHMAP_EVENT_CANDIDATES = "event_candidates_healthmap"
HEALTHMAP_EVENTS = "events_healthmap"
EMPRESI_EVENT_CANDIDATES = "event_candidates_empresi"
EMPRESI_EVENTS = "events_empresi"

GEONAMES_HIERARCHY_INFO_FILENAME = "geonames_hierarchy"




# ====================================================
# GRAPH
# ====================================================

# node type
NODE_TYPE_EVENT = "event"
NODE_TYPE_LOC_DISTRICT = "district"
NODE_TYPE_LOC_CITY = "city"
NODE_TYPE_LOC_REGION = "region"
NODE_TYPE_LOC_COUNTRY = "country"
NODE_TYPE_LOC_CONTINENT = "continent"
#NODE_TYPE_DATE = "date"
NODE_TYPE_DISEASE = "disease"
NODE_TYPE_DISEASE_SUBTYPE = "disease subtype"
NODE_TYPE_HOST = "host"
NODE_TYPE_HOST_SUBTYPE = "host subtype"
NODE_TYPE_SYMPTOM = "symptom"
NODE_TYPE_SYMPTOM_SUBTYPE = "symptom subtype"
NODE_TYPE_SOURCE = "source"
NODE_TYPE_POST = "post"

# node attributes
NODE_ATTR_DATE = "date"

# edge type
EDGE_TYPE_REPORTING = "reports/reported-by"
EDGE_TYPE_PUBLISHING = "publishes/published-by"
EDGE_TYPE_WHERE = "where" # >> location
EDGE_TYPE_WHO = "who" # >> host
EDGE_TYPE_WHAT = "what" # >> disease
EDGE_TYPE_HOW = "how" # >> symptom
EDGE_TYPE_HIERARCHY = "hierarchy"
EDGE_TYPE_TEMP_DIST = "temporal-distance"
EDGE_TYPE_COUNTRY_NEIGHBOR = "country-neighbor"
EDGE_TYPE_GEODESIC_DIST = "geodesic-distance"
EDGE_TYPE_TEMP_DIST = "temporal dist"

# edge attributes
EDGE_ATTR_WEIGHT = "weight"
EDGE_ATTR_SIGN = "sign"

# ====================================================
# COLUMN NAMES
# ====================================================
COL_ID = "id"
COL_ARTICLE_ID = "article_id"
COL_ARTICLE_ID_RENAMED = "id_articleweb"
COL_SIGNAL_ID = "signal_id"
COL_SIGNAL_ID_RENAMED = "id_signal"
COL_SIGNAL_DATE = "date_signal"
COL_SIGNAL_REF_EMPRESI = "ref_empresi"
COL_PATH_ID = "path_id"
COL_PATH_ID_RENAMED = "id_path"
COL_SUMMARY = "summary"
COL_LANG = "lang"
#COL_CLASSIF_LABEL = "classification_label"
COL_CLASSIF_LABEL_ID = "classificationlabel_id"
COL_ARTICLE_CLASSIF_LABEL_ID = "a_classificationlabel_id"
COL_SENTENCE_CLASSIF_LABEL_ID = "s_classificationlabel_id"
COL_START = "start"
COL_END = "end"
COL_TEXT = "text"
COl_POS = "position"
COl_TOKEN_INDEX = "token_index"
COl_LENGTH = "length"
COL_TYPE = "type"
COL_LABEL = "label"
COL_VALUE = "value"
COL_TITLE = "title"
COL_SENTENCES = "sentences"
COL_URL = "url"
COL_POST_ID = "post_id"
COL_SOURCE = "source"
COL_DESCR = "description"
COL_NAME = "name"
COL_PUBLISHED_TIME = "published_at"
COL_PROCESSED_TEXT = "processed_text"
COL_KEYW_ID = "keyword_id"
COL_KEYW_TYPE_ID = "keyword_type_id"
COL_ALIGN_KEYW_ID = "aligned_keyword_id"
COL_RSSFEED_ID = "id_rssfeed"
COL_GEONAMES_ID = "geonames_id"
COL_GEONAMS_JSON = "geonames_json"
COL_FROM_AUTO_EXTR = "from_automatic_extraction"
COL_SENTENCE_ID = "sentence_id"
COL_FEATURE_CODE = "feature_code"
COL_COUNTRY = "country"
COL_COUNTRY_ID = "id_country"
COL_COUNTRY_FREQ = "country_freq"
COL_NB_COUNTRY_FREQ = "nb_country"
COL_PUB_COUNTRY = "pub_country"
COL_COUNTRY_ALPHA2_CODE = "Alpha-2 code"
COL_COUNTRY_ALPHA3_CODE = "Alpha-3 code"

COL_LAT = "lat"
COL_LNG = "lng"



COL_LOC_DISTRICT = "district"
COL_LOC_CITY = "city"
COL_LOC_REGION = "region"
COL_LOC_COUNTRY = "country"
COL_LOC_CONTINENT = "continent"
COL_DISEASE = "disease"
COL_DISEASE_SUBTYPE = "disease subtype"
COL_HOST = "host"
COL_HOST_SUBTYPE = "host subtype"
COL_SYMPTOM = "symptom"
COL_SYMPTOM_SUBTYPE = "symptom subtype"

COL_ID_PATH_LIST = "id_path_list"

KEY_GEONAMES_COUNTRY = "country"
KEY_GEONAMES_CODE = "code"
KEY_GEONAMES_NAME = "name"
KEY_GEONAMES_STATE = "state"
KEY_GEONAMES_ADDRESS = "address"


# ====================================================
# EVAL MEASURES
# ====================================================

EVAL_FMEASURE = "fmeasure"
EVAL_PRECISION = "precision"
EVAL_RECALL = "recall"
EVAL_NMI = "nmi"
EVAL_ARI = "ari"
EVAL_RI = "ri"

# ====================================================
# RESULT FILE NAMES
# ====================================================
RESULT_FILENAME_EVENT_CLUSTERING = "event-clustering"
RESULT_FILENAME_NETWORK_ALIGNMENT = "network-alignment"
RESULT_FILENAME_DATAFRAME_ALIGNMENT = "dataframe-alignment"


# ====================================================
# FILE FORMAT
# ====================================================

FILE_FORMAT_CSV = "csv"
FILE_FORMAT_TXT = "txt"
FILE_FORMAT_PNG = "png"
FILE_FORMAT_PDF = "pdf"
FILE_FORMAT_SIGNED_GRAPH = "G"
FILE_FORMAT_GRAPHML = "graphml"



# ====================================================
# DISEASE NAMES
# ====================================================
DISEASE_AVIAN_INFLUENZA = "AI"
DISEASE_WEST_NILE_VIRUS = "WNV"





# ====================================================
# Continents
# ====================================================
CONTINENTS_CODE = ["AF", "NA", "OC", "AN", "AS", "EU", "SA"]
CONTINENTS_NAME = ["Africa", "North America", "Oceania", "Antarctica", "Asia", "Europe", "South America"]


# ====================================================
# GEONAMES
# ====================================================
MAX_NB_LOCS_PER_GEONAMES_REQUEST = 100 # by default from geonames

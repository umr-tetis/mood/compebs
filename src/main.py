
import os
import consts
from news_outlet_analysis.perform_news_outlet_analysis import perform_news_outlet_pagerank_analysis, perform_news_outlet_time_detection_analysis
from spatial_analysis.spatial_distribution_map import perform_spatial_distribution_map

from event_matching.event_matching import EventMatching
from event_matching.event_matching_strategy import EventMatchingStrategyPossiblyDuplicate, EventMatchingStrategyEventSimilarity

from util_event import simplify_df_events_at_hier_level1

from spatial_analysis.temporal_geo_coverge import process_temporal_geo_coverage_for_all_platforms, plot_temporal_geo_coverage_results_for_all_platforms, plot_temporal_geo_coverage_result_comparison_for_all_platforms

from temporal_analysis.process_timeliness_analysis import plot_timeliness_for_all_platforms
from temporal_analysis.plot_event_evolution import plot_event_evolution_for_all_platforms
from thematic_analysis.plot_chord_diagram import plot_chord_diagram_for_all_platforms


if __name__ == '__main__':
    
  
  #########################################
  # EMPRES-i
  #########################################
  input_event_folder_empres_i = consts.IN_EVENTS_EMPRESSI_FOLDER

  events_filepath_empresi = os.path.join(input_event_folder_empres_i, "events.csv")
     
  events_simplified_filepath_empresi = os.path.join(input_event_folder_empres_i, "events_simplified.csv")
  simplify_df_events_at_hier_level1(events_filepath_empresi, events_simplified_filepath_empresi)
     
      
  #########################################
  # PADI-Web
  #########################################
  input_event_folder_padiweb = consts.IN_EVENTS_PADIWEB_FOLDER
  out_news_outlet_padiweb_folder = consts.OUT_NEWS_OUTLET_ANALYSIS_PADIWEB_FOLDER
  
  event_candidates_filepath_padiweb = os.path.join(input_event_folder_padiweb, "event_candidates.csv")
  clustering_result_filepath_padiweb = os.path.join(input_event_folder_padiweb, "event-clustering.txt")
  events_filepath_padiweb = os.path.join(input_event_folder_padiweb, "events.csv")
  try:
    if not os.path.exists(out_news_outlet_padiweb_folder):
      os.makedirs(out_news_outlet_padiweb_folder)
  except OSError as err:
     print(err)
     
  events_simplified_filepath_padiweb = os.path.join(input_event_folder_padiweb, "events_simplified.csv")
  simplify_df_events_at_hier_level1(events_filepath_padiweb, events_simplified_filepath_padiweb)

  #for by_continent in ["EU", "AS", "NA", "AF", "SA", "-1"]: # "-1" for World
  for by_continent in ["AS", "-1"]: # "-1" for World
    print("calculate newslet pagerank scores in padiweb for continent:", by_continent)
    perform_news_outlet_pagerank_analysis(event_candidates_filepath_padiweb, \
                   clustering_result_filepath_padiweb, out_news_outlet_padiweb_folder, by_continent=by_continent)
    
    perform_news_outlet_time_detection_analysis(event_candidates_filepath_padiweb, \
                   clustering_result_filepath_padiweb, out_news_outlet_padiweb_folder, by_continent=by_continent)


  #########################################
  # ProMED
  #########################################
  input_event_folder_promed = consts.IN_EVENTS_PROMED_FOLDER
  out_news_outlet_promed_folder = consts.OUT_NEWS_OUTLET_ANALYSIS_PROMED_FOLDER
  
  event_candidates_filepath_promed = os.path.join(input_event_folder_promed, "event_candidates.csv")
  clustering_result_filepath_promed = os.path.join(input_event_folder_promed, "event-clustering.txt")
  events_filepath_promed = os.path.join(input_event_folder_promed, "events.csv")
  try:
    if not os.path.exists(out_news_outlet_promed_folder):
      os.makedirs(out_news_outlet_promed_folder)
  except OSError as err:
     print(err)
  
  events_simplified_filepath_promed = os.path.join(input_event_folder_promed, "events_simplified.csv")
  simplify_df_events_at_hier_level1(events_filepath_promed, events_simplified_filepath_promed)
  
  #for by_continent in ["EU", "AS", "NA", "AF", "SA", "-1"]: # "-1" for World
  for by_continent in ["AS", "-1"]: # "-1" for World
    print("calculate newslet pagerank scores in promed for continent:", by_continent)
    perform_news_outlet_pagerank_analysis(event_candidates_filepath_promed, \
                   clustering_result_filepath_promed, out_news_outlet_promed_folder, by_continent=by_continent)
    
    perform_news_outlet_time_detection_analysis(event_candidates_filepath_promed, \
                   clustering_result_filepath_promed, out_news_outlet_promed_folder, by_continent=by_continent)


  #########################################
  # Event matching between the considered EBS platforms
  #  1) PADI-Web - ProMED, 2) PADI-Web - EMPRES-i, 3) ProMED - EMPRES-i
  #########################################
  output_dirpath_event_matching = os.path.join(consts.OUT_FOLDER, "event_matching")
  try:
    if not os.path.exists(output_dirpath_event_matching):
      os.makedirs(output_dirpath_event_matching)
  except OSError as err:
     print(err)
  
  #event_matching_strategy = EventMatchingStrategyPossiblyDuplicate()
  event_matching_strategy = EventMatchingStrategyEventSimilarity()
  job_event_matching = EventMatching(event_matching_strategy)
  job_event_matching.perform_event_matching(consts.NEWS_SURVEILLANCE_PADIWEB,\
                                            events_filepath_padiweb,\
                                            consts.NEWS_SURVEILLANCE_PROMED,\
                                            events_filepath_promed,\
                                            output_dirpath_event_matching)
  
  job_event_matching.perform_event_matching(consts.NEWS_SURVEILLANCE_PADIWEB,\
                                            events_filepath_padiweb,\
                                            consts.NEWS_DB_EMPRESS_I,\
                                            events_filepath_empresi,\
                                            output_dirpath_event_matching)
  
  job_event_matching.perform_event_matching(consts.NEWS_SURVEILLANCE_PROMED,\
                                            events_filepath_promed,\
                                            consts.NEWS_DB_EMPRESS_I,\
                                            events_filepath_empresi,\
                                            output_dirpath_event_matching)
  
 
 
  #########################################
  # Spatial event distribution through map plotting >> ONLY country and region (ADM1) level
  #    we handle it in the same scale for all the platforms, for comparability purposes
  #  REMARK: It takes time. Moreover, it requires a large amount of space >> need to improve it
  #########################################
  # events_filepath_dict = {}
  # events_filepath_dict[consts.NEWS_SURVEILLANCE_PADIWEB] = events_filepath_padiweb
  # events_filepath_dict[consts.NEWS_SURVEILLANCE_PROMED] = events_filepath_promed
  # events_filepath_dict[consts.NEWS_DB_EMPRESS_I] = events_filepath_empresi
  #
  # out_spatial_analysis_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis")
  # try:
  #   if not os.path.exists(out_spatial_analysis_folder):
  #     os.makedirs(out_spatial_analysis_folder)
  # except OSError as err:
  #    print(err)
  #
  # out_event_distr_platform_folder = os.path.join(out_spatial_analysis_folder, "event_distribution", "<PLATFORM>")
  # for by_continent in ["World"]: # ["World", "EU", "NA", "AS"]
  #   print("plot spatial distribution map for continent:", by_continent)
  #   perform_spatial_distribution_map(events_filepath_dict, out_event_distr_platform_folder, by_continent=by_continent)
  
  
  #########################################
  # Spatio-temporal representativeness >> ONLY country and region (ADM1) level
  ######################################### 
  # PREPARATION
  out_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis", "spatiotemporal_representativeness")
  platforms = [consts.NEWS_SURVEILLANCE_PADIWEB, consts.NEWS_SURVEILLANCE_PROMED, consts.NEWS_DB_EMPRESS_I]
  spatial_scales = ["country", "region"] # "country", "region"
  temporal_scales = ["week_no", "month_no"] # "week_no", "month_no"
  process_temporal_geo_coverage_for_all_platforms(out_folder, platforms, spatial_scales, temporal_scales)
  
  # PLOT
  input_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis", "spatiotemporal_representativeness")
  out_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis", "spatiotemporal_representativeness")
  ref_platform = consts.NEWS_DB_EMPRESS_I
  platforms = [consts.NEWS_SURVEILLANCE_PADIWEB, consts.NEWS_SURVEILLANCE_PROMED]
  spatial_scales = ["country", "region"] # "country", "region"
  temporal_scales = ["week_no", "month_no"] # "week_no", "month_no"
  plot_temporal_geo_coverage_results_for_all_platforms(input_folder, out_folder, ref_platform, platforms, spatial_scales, temporal_scales)
  
  # PLOT
  input_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis", "spatiotemporal_representativeness")
  out_folder = os.path.join(consts.OUT_FOLDER, "spatial_analysis", "spatiotemporal_representativeness")
  ref_platform = consts.NEWS_DB_EMPRESS_I
  platforms = [consts.NEWS_SURVEILLANCE_PADIWEB, consts.NEWS_SURVEILLANCE_PROMED]
  spatial_scales = ["region"] # "country", "region"
  temporal_scales = ["week_no", "month_no"] # "week_no", "month_no"
  plot_temporal_geo_coverage_result_comparison_for_all_platforms(input_folder, out_folder, platforms, ref_platform, spatial_scales, temporal_scales)


  
  #########################################
  # Timeliness
  #########################################  
  input_folder = os.path.join(consts.OUT_FOLDER, "event_matching")
  output_folder = os.path.join(consts.OUT_FOLDER, "temporal_analysis", "timeliness")
  platforms = [consts.NEWS_SURVEILLANCE_PADIWEB, consts.NEWS_SURVEILLANCE_PROMED, consts.NEWS_DB_EMPRESS_I]
  plot_timeliness_for_all_platforms(input_folder, output_folder, platforms)



  #########################################
  # Temporal event patterns through the evolution of events >> heatmap
  ######################################### 
  padiweb_event_filepath = os.path.join(consts.IN_EVENTS_PADIWEB_FOLDER, "event_candidates.csv")
  promed_event_filepath = os.path.join(consts.IN_EVENTS_PROMED_FOLDER, "event_candidates.csv")
  empresi_event_filepath = os.path.join(consts.IN_EVENTS_EMPRESSI_FOLDER, "events.csv")
  padiweb_clustering_filepath = os.path.join(consts.IN_EVENTS_PADIWEB_FOLDER, "event-clustering.txt")
  promed_clustering_filepath = os.path.join(consts.IN_EVENTS_PROMED_FOLDER, "event-clustering.txt")
  output_folder = os.path.join(consts.OUT_FOLDER, "temporal_analysis", "temporal_patterns")
  countries_of_interest = "CHN,VNM,KOR" # these are alpha3 country codes >> it cannot be empty or "-1"
  #year_of_interest = "-1" # "-1" means the whole time period
  time_interval_col_name = "biweek_no"
  year_of_interest = "2019"
  # time_interval_col_name = "week_no"
  # time_interval_col_name = "month_no"
  
  plot_event_evolution_for_all_platforms(padiweb_event_filepath, promed_event_filepath, empresi_event_filepath, \
                                         padiweb_clustering_filepath, promed_clustering_filepath, \
                                         output_folder, countries_of_interest, year_of_interest, time_interval_col_name)
  
  

  #########################################
  # Thematic entity analysis through chord diagram >> circos
  #   In the plots, the spatial entities are at country level.
  #   Moreover, the thematic entities are at level 2 (e.g. Avian Influenza (AI) and HPAI)
  ######################################### 
  padiweb_event_filepath = os.path.join(consts.IN_EVENTS_PADIWEB_FOLDER, "events_simplified.csv")
  promed_event_filepath = os.path.join(consts.IN_EVENTS_PROMED_FOLDER, "events_simplified.csv")
  empresi_event_filepath = os.path.join(consts.IN_EVENTS_EMPRESSI_FOLDER, "events_simplified.csv")
  output_folder = os.path.join(consts.OUT_FOLDER, "thematic_analysis")
  countries_of_interest = "TW,IN,CH,VN,KR,JP" # these are alpha3 country codes >> it cannot be empty or "-1"
  year_of_interest = "2019" # "-1" means the whole time period
  time_interval_col_name = "week_no"
  # year_of_interest = "-1"
  # time_interval_col_name = "week_no"
  # time_interval_col_name = "month_no"
  
  plot_chord_diagram_for_all_platforms(padiweb_event_filepath, promed_event_filepath, empresi_event_filepath, \
                                         output_folder, countries_of_interest, year_of_interest, time_interval_col_name)
  
  
  

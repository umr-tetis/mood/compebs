'''
Created on Dec 5, 2022

@author: nejat
'''


import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

import consts
from itertools import combinations 



def plot_timeliness(platform1_name, platform2_name, diff_values, out_figure_filepath):
  fig = plt.figure(figsize=(10, 10))
  
  plt.plot(diff_values)
  plt.axhline(y=0, color='r', linestyle='-')
  
  plt.xlabel("Shared events in chronological order", fontsize=24)
  plt.ylabel("date("+ platform1_name + ") − date(" + platform2_name + ") in days", fontsize=24)
  plt.xticks(fontsize=20)
  plt.yticks(fontsize=20)
  
  fig.savefig(out_figure_filepath)



def plot_timeliness_for_all_platforms(input_folder, output_folder, platforms):
  
  try:
    if not os.path.exists(output_folder):
      os.makedirs(output_folder)
  except OSError as err:
     print(err)
  
  
  for platform_pair in list(combinations(platforms, 2)):
    platform1_name = platform_pair[0]
    platform2_name = platform_pair[1]
    alignment_filepath = os.path.join(input_folder, platform1_name+"_"+platform2_name+"_event_matching.csv")
    df_alignment = pd.read_csv(alignment_filepath, sep=";", keep_default_na=False)

    df_alignment[platform1_name+"_date"] = pd.to_datetime(df_alignment[platform1_name+"_date"])
    df_alignment[platform2_name+"_date"] = pd.to_datetime(df_alignment[platform2_name+"_date"])
    
    # sort the dataframe by the the date info of the padiweb events
    df_alignment.sort_values([platform1_name+"_date"], ascending=[True], inplace=True)

    platform1_date_values = df_alignment[platform1_name+"_date"].to_list()
    platform2_date_values = df_alignment[platform2_name+"_date"].to_list()
    
    diff_values = [(platform1_date_values[i]-platform2_date_values[i]).days for i in range(len(platform1_date_values))]

    #np_diff_values = np.array(diff_values)
    #print(platform_pair, len(np_diff_values))
    #print(platform_pair, len(np_diff_values[np_diff_values>0]), len(np_diff_values[np_diff_values<0]))
    #print(platform_pair, len(np_diff_values[np_diff_values>30]), len(np_diff_values[np_diff_values<-30]))
    
    out_figure_filepath = os.path.join(output_folder, "timeliness_" + platform1_name+"_"+platform2_name+".png")
  
    plot_timeliness(platform1_name, platform2_name, diff_values, out_figure_filepath)


  

  

import os
import consts
import numpy as np
import pandas as pd
import util


import event
from location import Location
from temporality import Temporality
from host import Host
from disease import Disease
from symptom import Symptom

from event_clustering import AbstractEventClustering
import geopandas as gpd
import networkx as nx

from split_event_data import retrieve_asia_df_event_candidates, retrieve_north_america_df_event_candidates, retrieve_europe_df_event_candidates


def read_df_event_candidates(events_filepath):

  # read data frame from csv file
  cols_event_candidates = [consts.COL_ID, consts.COL_ARTICLE_ID, consts.COL_URL, consts.COL_SOURCE, \
                           consts.COL_LOC_CITY, consts.COL_LOC_REGION, consts.COL_LOC_COUNTRY, \
                           consts.COL_PUBLISHED_TIME, consts.COL_DISEASE_SUBTYPE, consts.COL_DISEASE,  \
                           consts.COL_HOST_SUBTYPE, consts.COL_HOST, consts.COL_SYMPTOM_SUBTYPE, consts.COL_SYMPTOM, \
                           consts.COL_TITLE, consts.COL_SENTENCES, consts.COL_LAT, consts.COL_LNG, \
                           "clustering", "month_no", "week_no","year"]
  df_event_candidates = pd.read_csv(events_filepath, usecols=cols_event_candidates, sep=";")

  # handle the missing cells
  df_event_candidates[consts.COL_SYMPTOM] = df_event_candidates[consts.COL_SYMPTOM].replace(np.nan, "")
  df_event_candidates[consts.COL_SYMPTOM_SUBTYPE] = df_event_candidates[consts.COL_SYMPTOM_SUBTYPE].replace(np.nan, "")
  df_event_candidates[consts.COL_HOST] = df_event_candidates[consts.COL_HOST].replace(np.nan, "")
  df_event_candidates[consts.COL_HOST_SUBTYPE] = df_event_candidates[consts.COL_HOST_SUBTYPE].replace(np.nan, "")
  df_event_candidates[consts.COL_LOC_CITY] = df_event_candidates[consts.COL_LOC_CITY].replace(np.nan, "")
  df_event_candidates[consts.COL_LOC_REGION] = df_event_candidates[consts.COL_LOC_REGION].replace(np.nan, "")
  df_event_candidates[consts.COL_DISEASE_SUBTYPE] = df_event_candidates[consts.COL_DISEASE_SUBTYPE].replace(np.nan, "")
  
  # create time-related columns: "month" and "year" from date
  published_dates = pd.to_datetime(df_event_candidates[consts.COL_PUBLISHED_TIME])
  years = [int(published_dates[i].strftime("%Y")) for i in range(len(published_dates))]
  df_event_candidates["month_no"] = [str(df_event_candidates.iloc[i]["month_no"])+"_"+str(years[i]) for i in range(len(years))]
  df_event_candidates["week_no"] = [str(df_event_candidates.iloc[i]["week_no"])+"_"+str(years[i]) for i in range(len(years))]

  return df_event_candidates



def extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr):
  
  res_event_clustering = df_event_candidates["clustering"].to_numpy()
  print(res_event_clustering)
  clusters = set(res_event_clustering) # retrieve the unique ids
  print(clusters)
  
  event_canditates = []
  for index, row in df_event_candidates.iterrows():
    loc = Location(row[consts.COL_LOC_CITY],row[consts.COL_LOC_REGION],row[consts.COL_LOC_COUNTRY])
    t = Temporality(row[consts.COL_PUBLISHED_TIME])
    dis = Disease(row[consts.COL_DISEASE_SUBTYPE], row[consts.COL_DISEASE])
    h = Host()
    h.load_dict_data_from_str(row[consts.COL_HOST_SUBTYPE], row[consts.COL_HOST])
    sym = Symptom()
    sym.load_dict_data_from_str(row[consts.COL_SYMPTOM_SUBTYPE], row[consts.COL_SYMPTOM])
    e = event.Event(index, row[consts.COL_ARTICLE_ID], row[consts.COL_URL], \
                    row[consts.COL_SOURCE], loc, t, dis, h, sym, row[consts.COL_TITLE], row[consts.COL_SENTENCES])
    event_canditates.append(e)
  
  
  sources_event_dict = {}
  for e in event_canditates:
    if e.source not in sources_event_dict:
      sources_event_dict[e.source] = []
    sources_event_dict[e.source].append(e.e_id)
  print(sources_event_dict)
  
  counter = 0
  sources_dict = {}
  sources_dict_for_pandas = {"id": [], "source": []}
  for e in event_canditates:
    if e.source not in sources_dict:
      sources_dict[e.source] = counter
      sources_dict_for_pandas["id"].append(counter)
      sources_dict_for_pandas["source"].append(e.source)
      counter = counter + 1
      
  df_source_info = pd.DataFrame.from_dict(sources_dict_for_pandas)
  source_info_filepath = os.path.join(output_dirpath,"source_id_info_"+choice_strategy_descr+".txt")
  df_source_info.to_csv(source_info_filepath, index=True, sep=";")
  
  cluster_id_to_size = []
  
  adj_matrix_counting_cascade = np.zeros((len(sources_dict.keys()), len(clusters))) # publisher vs cascade
  print(adj_matrix_counting_cascade.shape)
  
  cluid_to_index = dict(zip(clusters,range(len(clusters))))
  for clu_str in clusters:
    print("------------------")
    clu_id = cluid_to_index[clu_str]
    print("clu_id: "+str(clu_id))
    event_candidate_list = list(np.array(event_canditates)[util.which(res_event_clustering==clu_str)])
    cluster_id_to_size.append(len(event_candidate_list))
    print(event_candidate_list)
    
    for e in event_candidate_list:
      source_id = sources_dict[e.source]
      adj_matrix_counting_cascade[source_id, clu_id-1] = 1
  
  print(adj_matrix_counting_cascade)
  np.savetxt(os.path.join(output_dirpath,"adj_matrix_sources_to_cascades_"+choice_strategy_descr+".txt"),adj_matrix_counting_cascade,fmt='%d',delimiter=';')
  
  # =========================================
  
  # create adjacency matrix of directed publishers network
  
  adj_matrix_fractional_counting_cascade = np.zeros((len(sources_dict.keys()), len(sources_dict.keys()))) # source vs cascade
  print(adj_matrix_fractional_counting_cascade.shape)
  
  
  for i in range(0,len(sources_dict.keys())):
    for j in range(0, len(sources_dict.keys())):
      if i<=j:
        #print(str(i)+","+str(j))
        row_i = adj_matrix_counting_cascade[i, :] / cluster_id_to_size
        row_j = adj_matrix_counting_cascade[j, :] / cluster_id_to_size
        res = np.dot(row_i,np.transpose(row_j))
        adj_matrix_fractional_counting_cascade[i,j] = res
        adj_matrix_fractional_counting_cascade[j,i] = res
      
  np.savetxt(os.path.join(output_dirpath,"adj_matrix_undirected_sources_to_sources_"+choice_strategy_descr+".txt"), adj_matrix_fractional_counting_cascade,fmt='%.2f',delimiter=';') 
  
  
  # =======================================================
  
  adj_matrix_directed_fractional_counting_cascade = np.zeros((len(sources_dict.keys()), len(sources_dict.keys()))) # source vs cascade
  print(adj_matrix_directed_fractional_counting_cascade.shape)
  
  for i in range(0,len(sources_dict.keys())):
    diag = adj_matrix_fractional_counting_cascade[i,i]
    for j in range(0, len(sources_dict.keys())):
      #print(str(i)+","+str(j))
      adj_matrix_directed_fractional_counting_cascade[i,j] = adj_matrix_fractional_counting_cascade[i,j] / diag
    adj_matrix_directed_fractional_counting_cascade[i,i] = 0 # do not allow any self loop
  
  np.savetxt(os.path.join(output_dirpath,"adj_matrix_directed_sources_to_sources_"+choice_strategy_descr+".txt"), adj_matrix_directed_fractional_counting_cascade,fmt='%.2f',delimiter=';')   

  graph = nx.from_numpy_matrix(adj_matrix_directed_fractional_counting_cascade, create_using=nx.DiGraph)
  for node in graph.nodes(data=True):
    node_id = node[0]
    node[1]["name"] = sources_dict_for_pandas["source"][node_id]
  
  graph_filepath = os.path.join(output_dirpath,"directed_newslet_cooccurence_"+choice_strategy_descr+".graphml")
  nx.write_graphml(graph, graph_filepath)

  #graph.remove_nodes_from(list(nx.isolates(graph)))
  #pr = nx.pagerank(graph)
  #print(pr)



def extractAsiaFractionalCountingCascadeGraphForPadiweb(single_event_classif_strategy, event_retrieval_strategy, event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_PADIWEB_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_ASIA_PADIWEB_FOLDER
  
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  # signal_filepath = os.path.join(consts.IN_PADIWEB_FOLDER, \
  #                         consts.PADIWEB_SIGNAL_CSV_FILENAME + "." + consts.FILE_FORMAT_CSV)
  # signal_info_exists = os.path.isfile(signal_filepath)
    
  choice_strategy_descr_prev = "task1=" + single_event_classif_strategy.get_description() \
                        + "_task2=" + event_retrieval_strategy.get_description()
  choice_strategy_descr = choice_strategy_descr_prev + "_task3=" + event_clustering.get_description()


  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_by time_interval" \
                               + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)
  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  asia_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_asia_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(asia_event_candidates_filepath):
    df_event_candidates = retrieve_asia_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(asia_event_candidates_filepath, sep=";")
  else:
    df_event_candidates = read_df_event_candidates(asia_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
  

def extractNorthAmericaFractionalCountingCascadeGraphForPadiweb(single_event_classif_strategy, event_retrieval_strategy, event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_PADIWEB_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_NORTH_AMERICA_PADIWEB_FOLDER
    
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  # signal_filepath = os.path.join(consts.IN_PADIWEB_FOLDER, \
  #                         consts.PADIWEB_SIGNAL_CSV_FILENAME + "." + consts.FILE_FORMAT_CSV)
  # signal_info_exists = os.path.isfile(signal_filepath)
    
  choice_strategy_descr_prev = "task1=" + single_event_classif_strategy.get_description() \
                        + "_task2=" + event_retrieval_strategy.get_description()
  choice_strategy_descr = choice_strategy_descr_prev + "_task3=" + event_clustering.get_description()


  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_by time_interval" \
                               + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)
  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  north_america_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_north_america_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(north_america_event_candidates_filepath):
    df_event_candidates = retrieve_north_america_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(north_america_event_candidates_filepath, sep=";")

  else:
    df_event_candidates = read_df_event_candidates(north_america_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
    

def extractEuropeFractionalCountingCascadeGraphForPadiweb(single_event_classif_strategy, event_retrieval_strategy, event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_PADIWEB_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_EUROPE_PADIWEB_FOLDER
  
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  # signal_filepath = os.path.join(consts.IN_PADIWEB_FOLDER, \
  #                         consts.PADIWEB_SIGNAL_CSV_FILENAME + "." + consts.FILE_FORMAT_CSV)
  # signal_info_exists = os.path.isfile(signal_filepath)
    
  choice_strategy_descr_prev = "task1=" + single_event_classif_strategy.get_description() \
                        + "_task2=" + event_retrieval_strategy.get_description()
  choice_strategy_descr = choice_strategy_descr_prev + "_task3=" + event_clustering.get_description()


  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_by time_interval" \
                               + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)
  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  europe_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_europe_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(europe_event_candidates_filepath):
    df_event_candidates = retrieve_europe_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(europe_event_candidates_filepath, sep=";")

  else:
    df_event_candidates = read_df_event_candidates(europe_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
 
 
  
def extractFractionalCountingCascadeGraphForPadiweb(single_event_classif_strategy, event_retrieval_strategy, event_clustering:AbstractEventClustering):
  
  try:
    if not os.path.exists(consts.INFLUENCE_MAX_PADIWEB_FOLDER):
      os.makedirs(consts.INFLUENCE_MAX_PADIWEB_FOLDER)
  except OSError as err:
     print(err)
     
  # signal_filepath = os.path.join(consts.IN_PADIWEB_FOLDER, \
  #                         consts.PADIWEB_SIGNAL_CSV_FILENAME + "." + consts.FILE_FORMAT_CSV)
  # signal_info_exists = os.path.isfile(signal_filepath)
    
  choice_strategy_descr_prev = "task1=" + single_event_classif_strategy.get_description() \
                        + "_task2=" + event_retrieval_strategy.get_description()
  choice_strategy_descr = choice_strategy_descr_prev + "_task3=" + event_clustering.get_description()
  event_candidates_filepath = os.path.join(consts.CSV_PADIWEB_FOLDER, \
                              consts.PADIWEB_EVENT_CANDIDATES + "_by time_interval" \
                               + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)
  output_dirpath = consts.INFLUENCE_MAX_PADIWEB_FOLDER
  
  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
 
 
 
 
def extractFractionalCountingCascadeGraphForHealthmap(event_clustering:AbstractEventClustering):
  try:
    if not os.path.exists(consts.INFLUENCE_MAX_HEALTHMAP_FOLDER):
      os.makedirs(consts.INFLUENCE_MAX_HEALTHMAP_FOLDER)
  except OSError as err:
     print(err)
     
  # signal_filepath = os.path.join(consts.IN_HEALTHMAP_FOLDER, \
  #                         consts.HEALTHMAP_SIGNAL_CSV_FILENAME + "." + consts.FILE_FORMAT_CSV)
  # signal_info_exists = os.path.isfile(signal_filepath)
    
  choice_strategy_descr = "task3=" + event_clustering.get_description()
  event_candidates_filepath = os.path.join(consts.CSV_HEALTHMAP_FOLDER, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "." + consts.FILE_FORMAT_CSV)
  output_dirpath = consts.INFLUENCE_MAX_HEALTHMAP_FOLDER
    
  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
      
      
      
def extractAsiaFractionalCountingCascadeGraphForHealthmap(event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_HEALTHMAP_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_ASIA_HEALTHMAP_FOLDER
  
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  choice_strategy_descr = "task3=" + event_clustering.get_description()
  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_by time_interval" + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)

  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  asia_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_asia_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(asia_event_candidates_filepath):
    df_event_candidates = retrieve_asia_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(asia_event_candidates_filepath, sep=";")

  else:
    df_event_candidates = read_df_event_candidates(asia_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
  
  
  
def extractNorthAmericaFractionalCountingCascadeGraphForHealthmap(event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_HEALTHMAP_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_NORTH_AMERICA_HEALTHMAP_FOLDER
  
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  choice_strategy_descr = "task3=" + event_clustering.get_description()
  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_by time_interval" + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)

  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  asia_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_north_america_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(asia_event_candidates_filepath):
    df_event_candidates = retrieve_north_america_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(asia_event_candidates_filepath, sep=";")

  else:
    df_event_candidates = read_df_event_candidates(asia_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
 
 
def extractEuropeFractionalCountingCascadeGraphForHealthmap(event_clustering:AbstractEventClustering):
  input_dirpath = consts.CSV_HEALTHMAP_FOLDER
  output_dirpath = consts.INFLUENCE_MAX_EUROPE_HEALTHMAP_FOLDER
  
  try:
    if not os.path.exists(output_dirpath):
      os.makedirs(output_dirpath)
  except OSError as err:
     print(err)
     
  choice_strategy_descr = "task3=" + event_clustering.get_description()
  event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_by time_interval" + "_" + choice_strategy_descr + "." + consts.FILE_FORMAT_CSV)

  df_event_candidates = read_df_event_candidates(event_candidates_filepath)
    
  europe_event_candidates_filepath = os.path.join(input_dirpath, \
                              consts.HEALTHMAP_EVENT_CANDIDATES + "_europe_by time_interval" \
                               + "_" + choice_strategy_descr + ".csv")
  if not os.path.exists(europe_event_candidates_filepath):
    df_event_candidates = retrieve_europe_df_event_candidates(df_event_candidates)
    df_event_candidates.to_csv(europe_event_candidates_filepath, sep=";")

  else:
    df_event_candidates = read_df_event_candidates(europe_event_candidates_filepath)

  extractFractionalCountingCascadeGraph(df_event_candidates, output_dirpath, choice_strategy_descr)
 
      